﻿using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.XR;

public class IntroSceneManager : MonoBehaviour
{
    public void ReturnToMainMenu()
    {
        SceneManager.LoadScene(0);
    }

    public void SkipIntro()
    {
        SceneManager.LoadScene(2);
    }
}
