﻿using System.Collections;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Video;
using UnityEngine.SceneManagement;

public class PlayVideoOnCanvas : MonoBehaviour
{
    public RawImage m_RawImage;
    public VideoPlayer m_VideoPlayer;
    //public AudioSource m_AudioSource;

    private void Awake()
    {
        StartCoroutine(IEnumPlayVideo());
    }
    void Start()
    {     
        m_VideoPlayer.loopPointReached += WhenVideoIsDone;
        m_RawImage.enabled = false;
    }

    IEnumerator IEnumPlayVideo()
    {
        m_VideoPlayer.Prepare();
        WaitForSeconds waitForSeconds = new WaitForSeconds(1);
        while (!m_VideoPlayer.isPrepared)
        {
            yield return waitForSeconds;
            m_RawImage.enabled = true;
            break;
        }

        m_RawImage.texture = m_VideoPlayer.texture;
        m_VideoPlayer.Play();
        //m_AudioSource.Play();
    }

    // When video is done playing
    private void WhenVideoIsDone(VideoPlayer vp)
    {
        SceneManager.LoadScene(2);
    }
}
