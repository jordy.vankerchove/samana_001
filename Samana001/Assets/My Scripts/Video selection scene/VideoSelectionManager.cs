﻿using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.XR;

public class VideoSelectionManager : MonoBehaviour
{
    private void Awake()
    {
        // Disable VR
        Screen.orientation = ScreenOrientation.LandscapeLeft;
        XRSettings.enabled = false;
    }

    public void Rolstoelgebruiker()
    {
        PlayerPrefs.SetInt("VideoNumber",1);
        PlayerPrefs.Save();
        SceneManager.LoadScene(1);
    }
    public void Slechtziende()
    {
        PlayerPrefs.SetInt("VideoNumber", 2);
        PlayerPrefs.Save();
        SceneManager.LoadScene(1);
    }
    public void Dementie()
    {
        PlayerPrefs.SetInt("VideoNumber", 3);
        PlayerPrefs.Save();
        SceneManager.LoadScene(1);
    }
}
