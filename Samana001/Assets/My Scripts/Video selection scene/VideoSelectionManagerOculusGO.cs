﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class VideoSelectionManagerOculusGO : MonoBehaviour
{
    [SerializeField] private Camera m_PlayerCamera;
    [SerializeField] private GameObject m_GazeRing;
    [SerializeField] private GameObject m_Reticle;
    [SerializeField] private GameObject m_VidSelectionCanvas;

    private AudioSource m_AudioSource;
    private Image m_Img;
    private bool m_IsSelectingButton = false;
    private bool m_isButtonSoundPLayed = false;

    private void Start()
    {
        m_Img = m_GazeRing.GetComponent<Image>();
    }

    void Update()
    {
        GazingRaycast();

        //Fill gaze reticle when selecting button press.
        if (m_IsSelectingButton)
        {
            m_Img.fillAmount += 0.7f * Time.deltaTime;
        }
    }

    // Gazing with raycasts.
    void GazingRaycast()
    {
        RaycastHit hit;

        if (Physics.Raycast(m_PlayerCamera.transform.position, m_PlayerCamera.transform.forward, out hit) && hit.collider.gameObject.tag == "GazeObject")
        {
            StartCoroutine(WaitOneSecond(hit));
            m_IsSelectingButton = true;
            m_Reticle.SetActive(false);
        }
        else
        {
            StopAllCoroutines();
            m_IsSelectingButton = false;
            m_Reticle.SetActive(true);
            m_Img.fillAmount = 0f;
        }
    }

    // Wait one second for a button confirm.
    IEnumerator WaitOneSecond(RaycastHit hit)
    {
        yield return new WaitForSeconds(1.5f);  

        // Check what video is selected.
        if (hit.collider.gameObject.name == "Rolstoelgebruik" && m_IsSelectingButton == true)
        {
            Rolstoelgebruiker();
        }
        else if (hit.collider.gameObject.name == "Slechtziendheid" && m_IsSelectingButton == true)
        {
            Slechtziende();
        }
        else if (hit.collider.gameObject.name == "Dementie" && m_IsSelectingButton == true)
        {
            Dementie();
        }
    }

    public void Rolstoelgebruiker()
    {
        PlayerPrefs.SetInt("VideoNumber", 1);
        PlayerPrefs.Save();
        SceneManager.LoadScene(1);
    }
    public void Slechtziende()
    {
        PlayerPrefs.SetInt("VideoNumber", 2);
        PlayerPrefs.Save();
        SceneManager.LoadScene(1);          
    }
    public void Dementie()
    {
        PlayerPrefs.SetInt("VideoNumber", 3);
        PlayerPrefs.Save();
        SceneManager.LoadScene(1);
    }
}
